#!/bin/bash

#This script detects the underlying host OS and runs my setup config accordingly
#This currently supports Debian, Fedora, and EndeavourOS.

### Detect user, reject root user 
if (( $EUID == 1 )); then
    echo "It is not advised to run this as root"
    exit
fi

echo "####################################################################"
echo "#This script is interactive and will prompt for your sudo password.#"
echo "#                  Pay attention or time out!                      #"
echo "####################################################################"
sleep 3

#Define packages for different distros and package managers

flatpaks=("com.github.tchx84.Flatseal" "com.google.Chrome" "com.mattjakeman.ExtensionManager" "com.slack.Slack" "net.supertuxkart.SuperTuxKart" "org.gnome.Boxes" "org.gnome.Builder" "org.gnome.PowerStats" "org.libreoffice.LibreOffice" "org.mozilla.firefox" "org.signal.Signal" "sh.cider.Cider" "us.zoom.Zoom")

pack_arch=(vim git htop tmux papirus-icon-theme flatpak debtap timeshift nfs-utils ruby rsync tldr glances neofetch gnome-tweaks chromium docker kubernetes minikube gnome-software-plugin-flatpak ncdu ttf-fira-mono ttf-fira-sans archlinux-wallpaper)

pack_deb=(vim git htop tmux rsync wget ruby tldr neoetch gnome-tweaks papirus-icon-theme gnome-software-plugin-flatpak docker-ce docker-ce-cli containerd.io docker-compose-plugin bash-completion arch-install-scripts ncdu)

pack_fed=(vim git htop tmux papirus-icon-theme podman timeshift nfs-utils ruby rsync tldr glances neofetch gnome-tweaks chromium google-noto-emoji-color-fonts helm kubernetes gnome-software-plugin-flatpak ncdu)

pack_deb_backports=(flatpak)

### Functions for repeat actions. Gotta keep it DRY, y'all.

function install_flatpaks {
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    for pak in "${flatpaks[@]}"; do
      flatpak install --noninteractive $pak
    done
  echo 'Restart for flatpaks to appear in shell.' 
 }

function make_alias {
  echo "alias kubectl='minikube kubectl --'" >> /home/"$USER"/.bashrc
  echo "alias docs-up='docker run -it --rm -d -p 4000:4000 registry.gitlab.com/gitlab-org/gitlab-docs:15.0 ; docker run -it --rm -d -p 4001:4000 docs/docker.github.io'" >> /home/"$USER"/.bashrc
}

function enable_docker {
  sudo usermod -aG docker "$USER" && sudo systemctl enable --now docker
}

#Debian repos

deb_repos=(contrib non-free)

##Detect OS and do stuff accordingly

###Fedora
if [[ $(cat /etc/os-release) =~ 'NAME="Fedora Linux"' ]]; then 
    echo "Found Fedora. Good on you for trying something new."
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    for pak in "${flatpaks[@]}"; do
      flatpak install "$pak"
    sudo dnf install "${pack_fed[@]}" 
done

###Debian
elif [[ $(cat /etc/os-release) =~ 'ID=debian' ]]; then
    echo "Found Debian. Fine choice."
    for repo in "${deb_repos[@]}"; do
      sudo apt-add-repository "$repo"; done
    echo "deb http://deb.debian.org/debian bullseye-backports main" | sudo tee /etc/apt/sources.list.d/bullseye-backports.list
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt update && sudo apt upgrade -y
    sudo apt -t bullseye-backports install "$pack_deb_backports" -y
    sudo apt install -y "$pack_deb"
    sudo usermod -aG docker "$USER" && sudo systemctl enable --now docker
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    for pak in "${flatpaks[@]}"; do
      flatpak install --noninteractive "$pak"
    done
    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    sudo install minikube-linux-amd64 /usr/local/bin/minikube
    echo "alias kubectl='minikube kubectl --'" >> /home/"$USER"/.bashrc
    echo "alias docs-up='docker run -it --rm -d -p 4000:4000 registry.gitlab.com/gitlab-org/gitlab-docs:15.0 ; docker run -it --rm -d -p 4001:4000 docs/docker.github.io'" >> /home/"$USER"/.bashrc
    echo "Restart for flatpaks to appear in shell."
    echo "Done."


###EndeavourOS
elif [[ $(cat /etc/os-release) =~ 'ID=endeavouros' ]]; then
    echo "Found EndeavourOS. Enjoy the AUR, my friend."
    yay -Syy && yay -S "${pack_arch[@]}"
    install_flatpaks
    make_alias
    enable_docker

###Other, currently unsupported
else
  echo "This script did not detect a supported Linux operating system."
fi
