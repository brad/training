#!/bin/bash

#Check for two arguments

if [[ $# -ne 2 ]]
then 
	echo "Usage: backup <source_dir> <dest_dir>"
	echo "Try again."
	exit 1
fi

#Check for rsync
if ! command -v rsync > /dev/null 2>&1
then
	echo "Install rsync to proceed."
	exit 2
fi

#Capture current date formatted in YYYY-MM-DD
current_date=$(date +%Y-%m-%d)
rsync_options="-avb --backup-dir $2/$current_date --delete --dry-run"

$(which rsync) $rsync_options $1 $2/current | tee backup_$current_date.log
