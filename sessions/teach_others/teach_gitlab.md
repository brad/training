## Teach GitLab

The goal of this project is to mentor aspiring engineers into a Support Engineer-ready state. This plan is authored by Caleb Cooper, and with his guidance and permission I have modified and taken some liberties to suit my mentee.

### Structure

Mentees will be assigned several GitLab-related tasks to complete, with mentor-planted obstacles to overcome along the way. Mentees will use their troubleshooting skills and online resources (GitLab docs and forums, StackOverflow, etc) to

### Prerequisites

Mentees going through this program will need a junior-admin level of system administration or operational experience. Prior Linux experience is beneficial but not strictly required.

### Prompt  

**1**:

>Welcome to Vaperware, Inc., the maker of the best software never released. You have been hired to run their GitLab server. The old administrator left a while ago and did not leave any notes. Your job is to fix the below problems so Vaperware developers can start doing more DevOps better. There is an old, and grumpy, sysadmin who is available over Zoom chat to answer insitutional knowledge questions. He knows nothing about GitLab or this GitLab server.

>1. GitLab doesn't start, start it and make sure it starts on reboot.
>2. GitLab is not using HTTPS, make it use a self-signed certificate.
>3. We don't know the password to log in as root to GitLab, reset it and log in to the web interface. Make sure registeration is off.
>4. GitLab version is old, update it to newest, make sure not to break it along the way!
>5. We want nightly backups sent to another server, script that. The target is nickbackup@git.technolibre.net, the root user on your server has an SSH key.
>6. Fix any other problems you find along the way. Security policy prevents you from running a root shell, even through sudo, but you can run sudo commands.

**2**:

>Vaporware, Inc., now has a new CIO. He was hired to shake things up and he is getting to that starting with GitLab. He wants the Vaporware, Inc., GitLab server moved to AWS and rebuilt to scale. No components can share the same server and anything that can be moved into one of AWS's managed services must be. Before our next maintenance window on Saturday, research how to break GitLab into its component pieces and which of them can go into AWS managed services. Also, make an AWS account if you don't already have one.
Also, he fired the entire security team and says each engineer has to be in charge of their own security. Make sure you set up the new GitLab infrastructure in a secure way, or you will be fired. However, no one is enforcing the old security policies anymore, so it is up to you to make the policies for your infrastructure.

### Training sessions  

Below is a tentative schedule for synchronous meetings. The rate at which mentees progress may vary. These sessions are "open-book" but they are expected to share the resources they consult.

<details>
  <summary>**Session 1**</summary>

Goal: Mentee to discover resource limitations of VM when attempting to stand up GitLab <oldest supported> 

**Mentor** 

- Stand up GCP/AWS/DigitalOcean Debian/Ubuntu VM with _fewer resources than [the guidelines in the docs_](https://docs.gitlab.com/ee/install/requirements.html). For instance, a DigitalOcean VM with 2GB of RAM and 30 GB of storage.
- On VM:
  - Create account for mentee
  - Request and insert mentee's public SSH key in `/home/$MENTEE/.ssh/authorized_keys`
  - Set user password
  - Restrict user's `sudo` access to just an editor (`nano`, `vim`, etc)
  - Fill VM storage to prevent Omnibus GitLab install. 1.5 gigs of available storage works. Example: `fallocate -l 28G /tmp/bigass_file.brad`
- Direct mentee to install GitLab package

**Mentee**  

Mentee is expected to:

- Access system
- Recognize their limited `sudo` privileges, grant themselves access to commands necessary to install GitLab. The mentee will expand their `sudo` access throughout this program, but remember that they are _not_ to grant themselves unrestricted admin access.
- Attempt to install GitLab <oldest supported>
  - Install should stall and fail due to insufficient memory and disk space
- Ideally, candidate will find [the docs to install GitLab in a memory-constrained environment](https://docs.gitlab.com/omnibus/settings/memory_constrained_envs.html). They may not; this is okay if they can troubleshoot and resolve the problems.
- Locate and remove large file
- Create and mount swap file
  - Make swap mount persistent by adding file to `fstab`

