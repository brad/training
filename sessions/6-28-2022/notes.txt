6/28/2022

- Modify Docker compose file to break PostgreSQL out into separate container - https://docs.gitlab.com/ee/raketasks/backup_restore.html#excluding-specific-directories-from-the-backup

Command: `sudo gitlab-backup create SKIP=uploads,builds,artifacts,lfs,terraform_state,registry,pages,repositories,packages`

Config docs: https://docs.gitlab.com/ee/administration/postgresql/standalone.html#setting-it-up

Need to: 
- Review docs about external db
- docker compose networking: Need to code in a network for communication between gl node and pg node
- Import DB backup from GL application node to PG node.

---  


