#Sources

- [Build and push](https://www.shellhacks.com/gitlab-ci-cd-build-docker-image-push-to-registry/)
- [See what's mounted from within container](https://stackoverflow.com/questions/30642844/how-to-list-docker-mounted-volumes-from-within-the-container)
- [Volume mounting](https://docs.docker.com/engine/reference/builder/#volume)
