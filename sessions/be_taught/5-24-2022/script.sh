#!/bin/bash 

declare -a volumes=( $(mount | grep ^/dev/ | awk '{print $3}' | grep -v ^/$) )

for i in ${volumes[*]}; do 
 if [[ -d "${i}" ]]; then 
   if [[ "$(ls -A ${i} | grep -v '*not-empty.tar')" ]] ; then #Default behavior is to match on something existing; no existence means no do.
     echo "Whoa whoa -- something is in ${i}! Saving that for you..." && \
     tar -c --exclude not-empty.tar -f ${i}/not-empty.tar -C ${i} .
   else
     echo "${i} is empty. Go back to sleep."
   fi
 fi
done

