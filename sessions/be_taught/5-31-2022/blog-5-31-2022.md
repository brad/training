Brad Sevy 
May 28, 20222

# Tars on tap - Scripting Docker volume operations

Sometimes we do things because they are useful. But mostly, we do thing because we can; because they engage our brains and stretch our capabilities, or because our trusted and esteemed mentors ask us to do seemingly-arbitrary learning drills that, while seeming silly on the surface, inevitable lead to solidification of new skills and long-term growth. Onward -- for science.

## Script

First, let's take a look at the script as a whole. Thence, we will break the script down into sections and explore what each command is doing in turn:

```sh
#!/bin/bash 

declare -a volumes=( $(mount | grep ^/dev/ | awk '{print $3}' | grep -v ^/$) )

for i in ${volumes[*]}; do 
 if [[ -d "${i}" ]]; then 
   if [[ ! -z "$(ls -A ${i} | grep -v '*not-empty.tar')" ]] ; then
     echo "Whoa whoa -- something is in ${i}! Saving that for you..." && \
     tar -c --exclude not-empty.tar -f ${i}/not-empty.tar -C ${i} .
   else
     echo "${i} is empty. Go back to sleep."
   fi
 fi
done
```
 
### Commands

To make sense of the script, let's briefly break it into its constituent parts and learn what each command does.

#### She bangs... or does hashbang?

**`#!/bin/bash`** 

The [shebang](https://en.wikipedia.org/wiki/Shebang_(Unix)), the first line of the script, defines the interpretter to be used to run the remainder of the script. Here I'm hardcoding the path of [the Bourne Again SHell](https://wiki.archlinux.org/title/Bash), but other shells or interpreters can be call here.

##### Daclaring an array 

**`declare -a volumes=( $(mount | grep ^/dev/ | awk '{print $3}' | grep -v ^/$) )`** 

Here, we are `declare`ing an array ( `-a` ) titled `volumes`. The contents of the array will be the output of the commands run in the [subshell](https://linuxhandbook.com/subshell/) beginning with `$(`. 

- `mount` prints active mountpoints on the system. Piped into;
- `grep ^/dev/` greps out lines that _begin_ ( `^` ) with `/dev/`, because we only want to see volumes mounted into the container, which Docker sees as devices. Piped into;
- `awk '{print $3}'` receives the output of the previous command and prints just the third word (delineated by whitespace by default) in each line. Piped into; 
- `grep -v ^/$` in**v**erse ( `-v` )`grep`s for the term  `^/$` to filter _out_ all lines which do _not_ begin and end with `/`. In [regex](https://linuxconfig.org/bash-regexps-for-beginners-with-examples), `^` == "begins with" and `$` == "ends with". A line that begins and ends with `/` is a line that contains _just_ `/`. Therefore, this command removes lines from output that contain just `/`.

**Okay, so** we are running these commands piped together to store mounted volume paths in an array called `volumes`; now what? Now we do stuff with it! 

#### Working with the array

Now that we have mounted volume directories stored in an array, we'll iterate through them to test whether they have content, and if they do, archive the contents and place the archive on the volume.

**`for i in ${volumes[*]}; do`** 

This [`for` loop](https://www.cyberciti.biz/faq/bash-for-loop/) iterates over each index in the `volumes` array, performing the command following `do`. The syntax here, `${volumes[*]}`, uses _curly braces_ to enumerate values.

**`if [[ -d "${i}" ]]; then`**  

If the value of current index of the `volumes` array is a _directory_ ( `-d` ), _then_ do what follows. `if` uses conditionals of the [`test` command](https://linux.die.net/man/1/test). 

**`if [[ ! -z "$(ls -A ${i} | grep -v '*not-empty.tar')" ]] ; then`**

This is a nested `if` statement that will execute if the condition "is not ( `!` ) zero ( `-z` )" is true against the command `ls -A ${i} | grep -v '*not-empty.tar'`. Thus, we end up with "if the results of the command `ls -A ${i} | grep -v '*not-empty.tar'` as non-zero, then do the next thing". The command itself `ls`s the directories stored in the `volumes` array, removes any lines that end with `not-empty.tar`, and if there is a non-zero output (read: if there is content in the directory besides the tar), moves to the next item line:  

**echo** 

```sh
echo "Whoa whoa -- something is in ${i}! Saving that for you..." && \`**  
tar -c --exclude not-empty.tar -f ${i}/not-empty.tar -C ${i} .
```

If contents are found in the statted directory, `echo` prints a message indicating that data is found. `tar` then creates a tarchive of the contents in the non-empty directory, within said directory. Tar excludes any previously-created `not-empty.tar` files and archives the rest of the files in the directory. To avoid recursively tarring, `tar` changes to the directory of the current index of the array and executes there.

**else** 

```sh
else
   echo "${i} is empty. Go back to sleep."
```  

If no content is found in the mounted directories, echo a message indicating nothing to be done.

**Closing the loop**  

Now, we close our `if` statements by, creatively, reversing the word to `fi`. We need to close both the parent and nested statements, as well as ending the `for` loop with `done`. 

```sh
    fi
  fi
done
``` 

## Conclusion  

In conclusion, we have seen how we can script the detection of volumes mounted into the currently-running Docker container and conditionally archive the contents of the volumes. You can learn more about the whole of this exercise by seeing my [`.gitlab-ci.yml`](../.gitlab-ci.yml) and my [Dockerfile](../Dockerfile). 

Next time on the Brad Blog, we will compare and contrast different pasta noodles. Don't bother liking/subscribing/donating. Now piss off and go outside.

:tada:
