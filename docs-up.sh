#! /bin/bash

#Serve GitLab and Docker docs locally

docker run --platform linux/amd64 -it --rm -d -p 4000:4000 registry.gitlab.com/gitlab-org/gitlab-docs:15.0
docker run --platform linux/amd64 -it --rm -d -p 4001:4000 docs/docker.github.io

echo "\ 
	GitLab docs are now available on localhost:4000, Docker docs at localhost:4001"
