Brad Sevy 
May 28, 20222

# Tars on tap - Scripting Docker volume operations

Sometimes we do things because they are useful. But mostly, we do thing because we can; because they engage and brains and stretch our capabilities, or because our trusted and esteemed mentors ask us to do seemingly-arbitrary learning drills that, while seeming silly on the surface, inevitable lead to solidification of new skills and long-term growth. Onward -- for science.

## Script

First, let's take a look at the script as a whole. Thence, we will break the script down into sections and explore what each command is doing in turn:

```
#!/bin/bash 

#Looks at volumes mounted within container, greps for mounts in /dev, prints just the third field of each line, and removes lines that start and end with `/`. Store it all in an array.

declare -a volumes=( $(mount | grep ^/dev/ | awk '{print $3}' | grep -v ^/$) )

#Iterate through each item in the array. If something is in mount, notify and tar items. If nothing is present, notify and do nothing further.

for i in ${volumes[*]}; do 
 if [[ -d "${i}" ]]; then 
   if [[ ! -z "$(ls -A ${i} | grep -v '*not-empty.tar')" ]] ; then
     echo "Whoa whoa -- something is in ${i}! Saving that for you..." && \
     tar -c --exclude not-empty.tar -f ${i}/not-empty.tar -C ${i} .
   else
     echo "${i} is empty. Go back to sleep."
   fi
 fi
done
```
 
### Commands

To make sense of the script, let's briefly break it into its constituent parts and learn what each command does.

**`#!/bin/bash`** - The -[shebang](https://en.wikipedia.org/wiki/Shebang_(Unix)), the first line of the script, defines the interpretter to be used to run the remainder of the script. In this script, I am hardcoding the path of the Bourne Again SHell; in other [examples](https://en.wikipedia.org/wiki/Shebang_(Unix)#Examples), 
