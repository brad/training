version: '3.6'

networks:
  srpg:
    internal: true

services:
  gitlab:
    environment:
      - COMPOSE_PROJECT_NAME = gitlab
      - GITLAB_SKIP_UNMIGRATED_DATA_CHECK = true
    image: 'gitlab/gitlab-ee:15.5.4-ee.0'
    hostname: 'gitlab'
    depends_on:
      - redis
      - postgres
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        logrotate['enable'] = false
        external_url 'http://localhost'
        letsencrypt['enable'] = false
        #POSTGRESQL
        postgresql['enable'] = false
        gitlab_rails['db_username'] = 'gitlab'
        gitlab_rails['db_password'] = 'gitlab'
        gitlab_rails['db_adapter'] = 'postgresql'
        gitlab_rails['db_encoding'] = 'unicode'
        gitlab_rails['db_host'] = 'postgres' # IP/hostname of database server
        #REDIS
        redis['enable'] = false
        gitlab_rails['redis_host'] = 'redis'
        gitlab_rails['redis_port'] = 6379
        gitlab_rails['redis_password'] = 'redis_password'
        #MONITORING - Disabling
        grafana['enable'] = false
        prometheus_monitoring['enable'] = false
        gitlab_kas['enable'] = false
        #SIDEKIQ - Comment out to move to corresponding node
        sidekiq['enable'] = false
        gitlab_rails['auto_migrate'] = true
        #GITALY
        gitaly['enable'] = false
        gitlab_rails['gitaly_token'] = 'abc123secret'
        gitlab_shell['secret_token'] = 'abc123secret'
        git_data_dirs({
          'default'  => { 'gitaly_address' => 'tcp://gitaly:8075' }})
        #RUNNER
        gitlab_rails['initial_shared_runners_registration_token'] = "tokentokentoken"
    ports:
      - '80:80'     #Nginx
      - '443:443'   #Nginx
      - '2222:22'   #SSH/gl-shell
#      - '5050:5050' #Registry
#      - '5432:5432' #PostgreSQL
#      - '8075:8075' #Gitaly
#      - '9090:9090' #Prometheus
#      - '6379:6379' #Redis TCP

    volumes:
      - '/srv/gitlab/config:/etc/gitlab'
      - '/srv/gitlab/logs/gitaly:/var/log/gitlab/gitaly'
      - '/srv/gitlab/logs/gitlab-workhorse:/var/log/gitlab/gitlab-workhorse'
      - '/srv/gitlab/logs/nginx:/var/log/gitlab/nginx'
      - '/srv/gitlab/logs/puma:/var/log/gitlab/puma'
      - '/srv/gitlab/logs/sshd-rails:/var/log/gitlab/sshd'
      - '/srv/gitlab/logs/logrotate-rails:/var/log/gitlab/logrotate'
      - '/srv/gitlab/data:/var/opt/gitlab/git-data'
    shm_size: '256m'

  postgres:
    image: 'gitlab/gitlab-ee:15.5.4-ee.0'
    hostname: 'postgres'
    environment:
      - COMPOSE_PROJECT_NAME = postgres
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        roles(['postgres_role'])
        prometheus['enable'] = false
        alertmanager['enable'] = false
        pgbouncer_exporter['enable'] = false
        redis_exporter['enable'] = false
        gitlab_exporter['enable'] = false
        postgresql['listen_address'] = '0.0.0.0'
        postgresql['port'] = 5432
        postgresql['sql_user'] = "gitlab"
        postgresql['sql_user_password'] = 'b7a289c0600988fe8e709dd2887e4d37'
        postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/32)
        postgresql['md5_auth_cidr_addresses'] = %w(0.0.0.0/0)
        gitlab_rails['auto_migrate'] = false

    volumes:
      - '/srv/gl-postgres:/var/opt/gitlab/postgresql'
      - '/srv/gitlab/logs/logrotate-pg:/var/log/gitlab/logrotate'
      - '/srv/gitlab/logs/sshd-pg:/var/log/gitlab/sshd'
      - '/srv/gitlab/logs/postgres-exporter:/var/log/gitlab/postgres-exporter'
      - '/srv/gitlab/logs/postgresql:/var/log/gitlab/postgresql'
    ports:
      - '5432:5432' #Postgres over TCP

#  gitlab-runner:
#    image: gitlab/gitlab-runner:alpine
#    environment:
#      REGISTER_NON_INTERACTIVE: "true"
#      REGISTER_RUN_UNTAGGED: "true"
#      CI_SERVER_URL: "https://gitlab.com"
#      REGISTRATION_TOKEN: "****REDACTED***"
#      RUNNER_EXECUTOR: "docker"
#      RUNNER_TAG_LIST: "cd,docker"
#      RUNNER_NAME: "pipeline-runner"
#    volumes:
#      - '/srv/gitlab-runner/config:/etc/gitlab-runner'

  redis:
    image: 'gitlab/gitlab-ee:15.5.4-ee.0'
#   restart: always
    hostname: 'redis'
    environment:
      - COMPOSE_PROJECT_NAME = redis
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        roles(['redis_master_role'])
        redis['bind'] = '0.0.0.0'
        redis['port'] = 6379
        redis['password'] = 'redis_password'
        gitlab_rails['auto_migrate'] = false
    volumes:
      - '/srv/gitlab/logs/sshd-redis:/var/log/gitlab/sshd'
      - '/srv/gitlab/logs/redis:/var/log/gitlab/redis'
      - '/srv/gitlab/logs/redis-exporter:/var/log/gitlab/redis-exporter'
      - '/srv/gitlab/logs/logrotate-redis:/var/log/gitlab/logrotate'
    ports:
      - '6379:6379' #Redis over TCP     
  sidekiq:
    image: 'gitlab/gitlab-ee:15.5.4-ee.0'
    hostname: 'sidekiq'
    environment:
      - COMPOSE_PROJECT_NAME = sidekiq
    depends_on:
      - gitlab
      - redis
      - postgres
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        logrotate['enable'] = false
        gitaly['enable'] = false
        postgresql['enable'] = false
        redis['enable'] = false
        nginx['enable'] = false
        puma['enable'] = false
        gitlab_workhorse['enable'] = false
        prometheus['enable'] = false
        alertmanager['enable'] = false
        grafana['enable'] = false
        gitlab_exporter['enable'] = false
        gitlab_kas['enable'] = false
        external_url 'http://localhost'
        gitlab_rails['internal_api_url'] = 'http://localhost'
        gitlab_shell['secret_token'] = 'abc123secret'
        #REDIS
        redis['master_password'] = 'redis_password'
        gitlab_rails['redis_host'] = 'redis'
        gitlab_rails['redis_port'] = 6379
        #GITALY
        git_data_dirs({
          "default" => {
            "gitaly_address" => "tcp://gitlab:8075",
            "gitaly_token" => "abc123token"
          }
        })
        #POSTGRES
        gitlab_rails['db_host'] = 'postgres'
        gitlab_rails['db_port'] = '5432'
        gitlab_rails['db_password'] = 'gitlab'
        gitlab_rails['auto_migrate'] = false
        #SIDEKIQ
        sidekiq['enable'] = true
        sidekiq['listen_address'] = "0.0.0.0"
        sidekiq['queue_groups'] = ['*'] * 4
        sidekiq['max_concurrency'] = 10
        #GITALY
        gitlab_shell['secret_token'] = 'shellsecret'
        gitaly['auth_token'] = 'abc123secret'
    volumes:
      - '/srv/gitlab/logs/logrotate-sidekiq:/var/log/gitlab/logrotate'
      - '/srv/gitlab/logs/sidekiq:/var/log/gitlab/sidekiq'
  gitaly:
    image: 'gitlab/gitlab-ee:15.5.4-ee.0'
    hostname: 'gitaly'
    environment:
      - COMPOSE_PROJECT_NAME = gitaly
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        gitaly['auth_token'] = 'abc123secret'
        postgresql['enable'] = false
        redis['enable'] = false
        nginx['enable'] = false
        puma['enable'] = false
        sidekiq['enable'] = false
        gitlab_workhorse['enable'] = false
        grafana['enable'] = false
        gitlab_exporter['enable'] = false
        gitlab_kas['enable'] = false
        gitlab_rails['auto_migrate'] = false
        prometheus['enable'] = false
        alertmanager['enable'] = false
        gitlab_rails['internal_api_url'] = 'http://localhost'
        gitaly['listen_addr'] = "0.0.0.0:8075"
        gitaly['auth_token'] = 'abc123secret'
        git_data_dirs({
          'default' => {
            'path' => '/var/opt/gitlab/git-data'
         },
        })
    volumes:
      - '/srv/gitlab/data/git-data:/var/opt/gitlab/git-data'
      - '/srv/gitlab/config:/etc/gitlab'
      - '/srv/gitlab/logs/logrotate-gitaly:/var/log/gitlab/logrotate'
      - '/srv/gitlab/logs/gitaly:/var/log/gitlab/sidekiq'
    ports:
      - '8075:8075' #Gitaly over TCP

