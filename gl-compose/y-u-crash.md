Docker compose crash

##Summary

Okay. Something goofy is happening and I need to figure out what. I try to launch my compose config, GL and Sidekiq crash within a few minutes. Launching again without shutting down the remaining nodes in the cluster sometimes works to bring up the service. _Sometimes_. 

First deploy worked properly 11:54 AM ET 9/20/2022
However, unable to import project from template with error `Error importing repository  into gitlab-instance-8b04cec6/spring - No such file or directory @ rb_file_s_lstat - [FILTERED]` -- persists with two different project imports.

###Troubleshooting
- GL, PG, and Sidekiq all exited by 12:02 😩
- 12:08 Ran `docker-compose up` again. Only container running was Redis
- 12:09 available in browser, able to sign in.
- Still cannot import repositories; same error as what's seen above. WTF, m8?
- 12:17 - As an experiment to see if increasing time between node creation has an effect `sudo rm -R /srv/gitlab /srv/gl-postgres; docker-compose up postgres -d; sleep 30; docker-compose up redis -d; sleep 30; docker-compose up sidekiq -d; sleep 30; docker-compose up -d`
- 12:19 - Nope. Only launched postgres then redis, but PG crashed :angry:
- 12:27 - GitLab/rails node now crashing like a little bastard. Oh and look Sidekiq too as I typed this.

**Starting again** 
Starting again, will clear GL data and tee docker logs into file to examine. Sigh. 😠 Sidekiq and redis still running

I can't make sense of why each node is crashing when it does. Let's go log-diving...



- 12:29 - Started cluster. Postgres crashed first, then shortly after Rails went down.
- 12:32 - started again, fresh data dirs, capturing logs.
- 12:32 Postgres dies.
- 12:33 - GL dies.

```
5813 gl-compose-gitlab-1    |       ^[[0m
5814 gl-compose-gitlab-1    |       ================================================================================^[[0m
5815 gl-compose-gitlab-1    |       ^[[31mError executing action `run` on resource 'ruby_block[restart_log_service]'^[[0m
5816 gl-compose-gitlab-1    |       ================================================================================^[[0m
5817 gl-compose-gitlab-1    |
5818 gl-compose-gitlab-1    | ^[[0m      Mixlib::ShellOut::ShellCommandFailed^[[0m
5819 gl-compose-gitlab-1    |       ------------------------------------^[[0m
5820 gl-compose-gitlab-1    |       Expected process to exit with [0], but received '1'
5821 gl-compose-gitlab-1    | ^[[0m      ---- Begin output of /opt/gitlab/embedded/bin/sv restart /opt/gitlab/service/logrotate/log ----
5822 gl-compose-gitlab-1    | ^[[0m      STDOUT: timeout: down: /opt/gitlab/service/logrotate/log: 1s, normally up, want up
5823 gl-compose-gitlab-1    | ^[[0m      STDERR:
5824 gl-compose-gitlab-1    | ^[[0m      ---- End output of /opt/gitlab/embedded/bin/sv restart /opt/gitlab/service/logrotate/log ----
5825 gl-compose-gitlab-1    | ^[[0m      Ran /opt/gitlab/embedded/bin/sv restart /opt/gitlab/service/logrotate/log returned 1^[[0m
``` 

Until later. Sigh.
