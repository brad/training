# Training

This repository is for content generated during my training sessions with Caleb. :party_parrot:

### Challenge 

**Part 1**

1. Create Dockerfile that builds image that when run as container tars the volume and places the tar on the volume.
1. If there is no content on mount, print error
1. Use GitLab CI to build image
1. Upload image to GitLab container registry from job

**Part 2** 

Comment script so people can make sense of it!
