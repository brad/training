#!/bin/bash 

#Looks at volumes mounted within container, greps for mounts in /dev, prints just the third field of each line, and removes lines that start and end with `/`. Store it all in an array.

declare -a volumes=( $(mount | grep ^/dev/ | awk '{print $3}' | grep -v ^/$) )

#Iterate through each item in the array. If something is in mount, notify and tar items. If nothing is present, notify and do nothing further.

for i in ${volumes[*]}; do 
 if [[ -d "${i}" ]]; then 
   if [[ ! -z "$(ls -A ${i} | grep -v '*not-empty.tar')" ]] ; then
     echo "Whoa whoa -- something is in ${i}! Saving that for you..." && \
     tar -c --exclude not-empty.tar -f ${i}/not-empty.tar -C ${i} .
   else
     echo "${i} is empty. Go back to sleep."
   fi
 fi
done

